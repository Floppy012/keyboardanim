#include "HIDHandle.hpp"
#include "HIDConnector.hpp"


bool HIDHandle::send(const byte_buffer_t &data) {
  if (data.empty()) return false;
  if (!ensureOpen()) return false;

  int result = hid_write(handle, const_cast<unsigned char*>(data.data()), data.size());
  hid_write(handle, 0x00, 1);

  // Utils::coutVector(data, true);

  // std::cout << std::endl;

  return !(result < 0);
}

bool HIDHandle::ensureOpen() {
  //if (handle != nullptr && hidOpen && hid_error(handle) == nullptr) return true;
  return open(true);
}

bool HIDHandle::open(bool force) {
  if (hidOpen && !force) return true;
  if (force) close();

  // Ensuer HID-API initialized
  //HIDConnector::initHID();

  //handle = hid_open_path(info->path.c_str());
  handle = hid_open(info->vendorID, info->productID, nullptr);

  if (handle == 0) return false;

  hidOpen = true;
  return true;
}

bool HIDHandle::close() {
  if (!hidOpen) return true;
  hidOpen = false;

  hid_close(handle);
  handle = nullptr;
  return true;
}
