#ifndef __LEDV2_H__
#define __LEDV2_H__

#include <map>

#include "Protocol.hpp"
#include "utils.hpp"
#include "BufferBuilder.hpp"
#include "ChunkedBufferBuilder.hpp"

class LEDV2 : public Protocol {

  public:
    LEDV2(const int address) : base_address(address) {};
    byte_buffer_t updateSingle(uint8_t key, int color) override;
    std::vector<byte_buffer_t> updateMulti(const std::vector<uint8_t> &keys, int color) override;
    std::vector<byte_buffer_t> updateMulti(const std::map<uint8_t, int> &keyColors) override;
    byte_buffer_t updateRange(uint8_t start, uint8_t end, int color) override;
    byte_buffer_t commit() override;


    enum Action {
      QUAD = 0x1A,
      CONSEC_FIVE = 0x2A,
      TRIPLE_RANGE = 0x3A,
      UNKNOWN_A = 0x4A,
      UNKNOWN_B = 0x5A,
      BULK = 0x6A,
      COMMIT = 0x7A,
      TERMINATE = 0xFF,
    };


  private:
    int base_address = 0;

    BufferBuilder *buffer() {
      return BufferBuilder::create(base_address, Action::TERMINATE);
    }

    ChunkedBufferBuilder *chunkedBuffer() {
      return ChunkedBufferBuilder::create(20, base_address, Action::TERMINATE);
    }
};


#endif // __LEDV2_H__