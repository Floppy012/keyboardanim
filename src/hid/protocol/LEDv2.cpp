#include "LEDv2.hpp"

byte_buffer_t LEDV2::updateSingle(uint8_t key, int color) {
  return buffer()
    ->action(Action::QUAD)
    ->push_back(key)
    ->color(color)
    ->finish(true);
}

std::vector<byte_buffer_t> LEDV2::updateMulti(const std::vector<uint8_t> &keys, int color) {
  return chunkedBuffer()
    ->action(Action::BULK)
    ->color(color)
    ->lockTemplate()
    ->push_back(keys)
    ->finishChunked();
}

std::vector<byte_buffer_t> LEDV2::updateMulti(const std::map<uint8_t, int> &keyColors) {
  ChunkedBufferBuilder *bb = chunkedBuffer()
    ->action(Action::QUAD)
    ->lockTemplate();

  for (const auto pair : keyColors) {
    bb->push_back(pair.first)
      ->color(pair.second);
  }

  return bb->finishChunked(true);
}

byte_buffer_t LEDV2::updateRange(uint8_t start, uint8_t end, int color) {
  /* Original "Range" Protocol version seems to be wrongly documented */
  // return buffer()
  //   ->action(Action::TRIPLE_RANGE)
  //   ->push_back(start)
  //   ->push_back(end)
  //   ->color(color)
  //   ->finish();
  return byte_buffer_t {};
}

byte_buffer_t LEDV2::commit() {
  return buffer()->action(Action::COMMIT)->finish();
}