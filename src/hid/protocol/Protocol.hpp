#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

#include "utils.hpp"

class Protocol {

  public:
    virtual byte_buffer_t updateSingle(uint8_t key, int color) = 0;
    virtual std::vector<byte_buffer_t> updateMulti(const std::vector<uint8_t> &keys, int color) = 0;
    virtual std::vector<byte_buffer_t> updateMulti(const std::map<uint8_t, int> &keyColors) = 0;
    virtual byte_buffer_t updateRange(uint8_t start, uint8_t end, int color) = 0;
    virtual byte_buffer_t commit() = 0;

};

#endif // __PROTOCOL_H__