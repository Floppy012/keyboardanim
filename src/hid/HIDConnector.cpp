#include <stdexcept>
#include <iostream>

#include "hidapi/hidapi.h"
#include "HIDConnector.hpp"


std::vector<KeyboardInfo> HIDConnector::getAvailableKeyboards() {
  std::vector<KeyboardInfo> out;

  initHID();

  struct hid_device_info *devs, *dev;
    devs = hid_enumerate(0x0, 0x0);
    dev = devs;

  while (dev) {
    if (dev->vendor_id != 0x46d) {
      dev = dev->next;
      continue;
    }

    KeyboardInfo info;

    info.vendorID = dev->vendor_id;
    info.productID = dev->product_id;
    info.path = dev->path;

    if (dev->serial_number) info.serialNumber = Utils::fromWchar(dev->serial_number);
    if (dev->manufacturer_string) info.manufacturer = Utils::fromWchar(dev->manufacturer_string);
    if (dev->product_string) info.product = Utils::fromWchar(dev->product_string);

    out.push_back(info);

    if (dev != nullptr) dev = dev->next;
  }

  hid_free_enumeration(devs);
  exitHID();

  return out;
}

HIDHandle *HIDConnector::getHandle(const KeyboardInfo *kbd) {
  HIDHandle *handle = new HIDHandle(kbd);
  handle->open();
  return handle;
}

void HIDConnector::initHID() {
  if (HID_ACTIVE) return;

  int result = hid_init();
  if (result < 0) {
    throw std::runtime_error(&"Error while intializing HID Library. init returned " [result]);
  }

  HID_ACTIVE = true;
}

void HIDConnector::exitHID() {
  if (!HID_ACTIVE) return;

  int result = hid_exit();
  if (result < 0) {
    throw std::runtime_error(&"Error while shutting down HID Library. exit returned " [result]);
  }

  HID_ACTIVE = false;
}
