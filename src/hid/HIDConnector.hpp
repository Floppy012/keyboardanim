#ifndef __HIDCONNECTOR_H__
#define __HIDCONNECTOR_H__

#include "KeyboardInfo.hpp"
#include "HIDHandle.hpp"

static bool HID_ACTIVE = false;

class HIDConnector {

  public:
    static HIDHandle *getHandle(const KeyboardInfo *kbd);
    static std::vector<KeyboardInfo> getAvailableKeyboards();
    static void initHID();
    static void exitHID();

};

#endif // __HIDCONNECTOR_H__