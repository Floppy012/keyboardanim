#ifndef __HIDHANDLE_H__
#define __HIDHANDLE_H__

#include "hidapi/hidapi.h"
#include "utils.hpp"
#include "KeyboardInfo.hpp"

class HIDHandle {

  public:
    HIDHandle(const KeyboardInfo *info) : info(info) {};
    ~HIDHandle() {
      close();
    }

    virtual bool send(const byte_buffer_t &data);
    virtual bool ensureOpen();
    virtual bool open(bool force = false);
    virtual bool close();

  private:
    const KeyboardInfo *info;
    bool hidOpen = false;
    hid_device *handle;

};

#endif // __HIDHANDLE_H__