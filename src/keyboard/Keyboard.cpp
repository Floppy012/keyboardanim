#include "Keyboard.hpp"
#include "G815.hpp"
#include <boost/algorithm/string.hpp>

KeyboardMapping * Keyboard::defaultMapping() {
  return &default_mapping;
}

KeyboardMapping * Keyboard::newMapping(const KeyboardType &kbType) {
  KeyboardMapping *mapping = new KeyboardMapping(kbType, &default_mapping);
  mappings[kbType] = mapping;

  return mapping;
}

uint8_t Keyboard::mapKey(const KeyboardType &kbdType, const std::string &key) {
  KeyboardMapping *mapping = &default_mapping;

  auto it = mappings.find(kbdType);
  if (it != mappings.end()) {
    mapping = it->second;
  }

  return mapping->map(boost::to_upper_copy(key));
}


bool Keyboard::close() {
  return handle->close();
}

void Keyboard::tick(const uint64_t time, const uint64_t tickCount) {
  animLock.lock();
  for (auto *anim : animations) {
    anim->tick(time, tickCount, this);
  }
  animLock.unlock();
}

bool Keyboard::setKeyColor(uint8_t key, int color) {
  byte_buffer_t buf = getProtocol()->updateSingle(key, color);
  return handle->send(buf);
}


bool Keyboard::setRangeColor(uint8_t rangeStart, uint8_t rangeEnd, int color) {
  //byte_buffer_t buf = getProtocol()->updateRange(rangeStart, rangeEnd, color);
  std::vector<uint8_t> range;
  Utils::addRange(rangeStart, rangeEnd, range);

  std::vector<byte_buffer_t> buffers = getProtocol()->updateMulti(range, color);
  
  bool out = false;

  for (const auto &buf : buffers) {
    out |= handle->send(buf);
  }

  return out;
}

bool Keyboard::setVectorColor(const std::vector<uint8_t> &keys, int color) {
  std::vector<byte_buffer_t> buffers = getProtocol()->updateMulti(keys, color);

  bool out = false;

  for (const auto &buf : buffers) {
    out |= handle->send(buf);
    // out |= this->commit();
    // std::cout << "    PART" << std::endl;
  }

  // std::cout << "Finish" << std::endl;

  return out;
}

bool Keyboard::blackout() {
  return setAllKeysColor(0);
}

bool Keyboard::commit() {
  byte_buffer_t buf = getProtocol()->commit();
  return handle->send(buf);
}


void Keyboard::addAnimation(Animation *anim) {
  animLock.lock();
  animations.push_back(anim);
  animLock.unlock();
}

Keyboard * Keyboard::instantiate(const KeyboardInfo &kbd) {
  switch (kbd.productID) {
    case GKeyboards::G815_KBD:
      return new G815(kbd);
    default:
      return nullptr;
  }
}