#include "G815.hpp"
#include "LEDv2.hpp"

G815::G815(const KeyboardInfo &kbdInfo) : 
  Keyboard(KeyboardMapping(KeyboardType::QWERTY_104, nullptr), kbdInfo),
  protocol(new LEDV2((0x11 << 16) | (0xFF << 8) | 0x10))
{
  registerMappings();
}

bool G815::setAllKeysColor(const int color) {
  bool out = false;
  out |= Keyboard::setRangeColor(toByte(KeyMap::A), toByte(KeyMap::NUM_DOT), color);
  out |= Keyboard::setRangeColor(toByte(KeyMap::LEFT_CTRL), toByte(KeyMap::RIGHT_WIN), color);
  out |= Keyboard::setRangeColor(toByte(KeyMap::MEDIA_PLAY), toByte(KeyMap::MEDIA_PREV), color);
  out |= Keyboard::setRangeColor(toByte(KeyMap::G1), toByte(KeyMap::G5), color);
  out |= Keyboard::setKeyColor(toByte(KeyMap::CONTEXT), color);
  out |= Keyboard::setKeyColor(toByte(KeyMap::DIM_BUTTON), color);
  out |= Keyboard::setKeyColor(toByte(KeyMap::LOGO), color);

  return out;
}

Protocol* G815::getProtocol() {
  return protocol;
}

std::vector<uint8_t> G815::getAllKeys() {
  std::vector<uint8_t> out;

  Utils::addRange(toByte(KeyMap::A), toByte(KeyMap::RIGHT_WIN), out);
  Utils::addRange(toByte(KeyMap::MEDIA_PLAY), toByte(KeyMap::MEDIA_PREV), out);
  Utils::addRange(toByte(KeyMap::G1), toByte(KeyMap::G5), out);
  out.push_back(toByte(KeyMap::DIM_BUTTON));
  out.push_back(toByte(KeyMap::LOGO));

  return out;
}

void G815::registerMappings() {
  registerDefaultMapping();
  registerQUERTZ105Mapping();
}

void G815::registerDefaultMapping() {
// A - Z
  this->defaultMapping()->registerMappings({
    SKV {"A", toByte(KeyMap::A)}, SKV {"B", toByte(KeyMap::B)}, SKV {"C", toByte(KeyMap::C)},
    SKV {"D", toByte(KeyMap::D)}, SKV {"E", toByte(KeyMap::E)}, SKV {"F", toByte(KeyMap::F)},
    SKV {"G", toByte(KeyMap::G)}, SKV {"H", toByte(KeyMap::H)}, SKV {"I", toByte(KeyMap::I)},
    SKV {"J", toByte(KeyMap::J)}, SKV {"K", toByte(KeyMap::K)}, SKV {"L", toByte(KeyMap::L)},
    SKV {"M", toByte(KeyMap::M)}, SKV {"N", toByte(KeyMap::N)}, SKV {"O", toByte(KeyMap::O)},
    SKV {"P", toByte(KeyMap::P)}, SKV {"Q", toByte(KeyMap::Q)}, SKV {"R", toByte(KeyMap::R)}, 
    SKV {"S", toByte(KeyMap::S)}, SKV {"S", toByte(KeyMap::S)}, SKV {"T", toByte(KeyMap::T)}, 
    SKV {"U", toByte(KeyMap::U)}, SKV {"V", toByte(KeyMap::V)}, SKV {"W", toByte(KeyMap::W)}, 
    SKV {"X", toByte(KeyMap::X)}, SKV {"Y", toByte(KeyMap::Y)}, SKV {"Z", toByte(KeyMap::Z)},
  });

  // Special chars
  this->defaultMapping()->registerMappings({
    // German: ^
    SKV {"`", toByte(KeyMap::BACK_TICK)},
    // German: <>|
    SKV {"\\", toByte(KeyMap::BACKSLASH)},

    SKV {",", toByte(KeyMap::COMMA)},
    SKV {".", toByte(KeyMap::DOT)},

    // German: -
    SKV {"/", toByte(KeyMap::FORWARDSLASH)},

    // German: ö
    SKV {";", toByte(KeyMap::SEMICOLON)},

    // German: ä
    SKV {"'", toByte(KeyMap::SINGLE_QUOTE)},
    
    // What about the german # key? Fuck 104 keyed keyboards

    // German: ü
    SKV {"[", toByte(KeyMap::OPEN_BRACKET)},

    // German: +
    SKV {"]", toByte(KeyMap::CLOSE_BRACKET)},

    // German: ß
    SKV {"-", toByte(KeyMap::DASH)},

    // German: `
    SKV {"=", toByte(KeyMap::EQUAL)},

    SKV {"SPACE", toByte(KeyMap::SPACE)},
  });

  // Number row (top)
  this->defaultMapping()->registerMappings({
    SKV {"1", toByte(KeyMap::N1)}, SKV {"2", toByte(KeyMap::N2)},
    SKV {"3", toByte(KeyMap::N3)}, SKV {"4", toByte(KeyMap::N4)},
    SKV {"5", toByte(KeyMap::N5)}, SKV {"6", toByte(KeyMap::N6)},
    SKV {"7", toByte(KeyMap::N7)}, SKV {"8", toByte(KeyMap::N8)},
    SKV {"9", toByte(KeyMap::N9)}, SKV {"0", toByte(KeyMap::N0)},
  });

  // Num Pad
  this->defaultMapping()->registerMappings({
    SKV {"NUM1", toByte(KeyMap::NUM1)}, SKV {"NUM2", toByte(KeyMap::NUM2)},
    SKV {"NUM3", toByte(KeyMap::NUM3)}, SKV {"NUM4", toByte(KeyMap::NUM4)},
    SKV {"NUM5", toByte(KeyMap::NUM5)}, SKV {"NUM6", toByte(KeyMap::NUM6)},
    SKV {"NUM7", toByte(KeyMap::NUM7)}, SKV {"NUM8", toByte(KeyMap::NUM8)},
    SKV {"NUM9", toByte(KeyMap::NUM9)}, SKV {"NUM0", toByte(KeyMap::NUM0)},
    SKV {"NUM_DOT", toByte(KeyMap::NUM_DOT)}, SKV {"NUM_ENTER", toByte(KeyMap::NUM_ENTER)},
    SKV {"NUM_PLUS", toByte(KeyMap::NUM_PLUS)}, SKV {"NUM_MINUS", toByte(KeyMap::NUM_MINUS)},
    SKV {"NUM_ASTERISK", toByte(KeyMap::NUM_ASTERISK)}, SKV {"NUM_FORWARDSLASH", toByte(KeyMap::NUM_FORWARDSLASH)},
    SKV {"NUM_LOCK", toByte(KeyMap::NUM_LOCK)},
  });

  // Control Keys
  this->defaultMapping()->registerMappings({
    SKV {"ESC", toByte(KeyMap::ESC)}, SKV {"TAB", toByte(KeyMap::TAB)},
    SKV {"CAPS", toByte(KeyMap::CAPS)}, SKV {"LEFT_SHIFT", toByte(KeyMap::LEFT_SHIFT)}, 
    SKV {"LEFT_CTRL", toByte(KeyMap::LEFT_CTRL)}, SKV {"LEFT_WIN", toByte(KeyMap::LEFT_WIN)}, 
    SKV {"LEFT_ALT", toByte(KeyMap::LEFT_ALT)}, SKV {"RIGHT_ALT", toByte(KeyMap::RIGHT_ALT)}, 
    SKV {"RIGHT_WIN", toByte(KeyMap::RIGHT_WIN)}, SKV {"CONTEXT", toByte(KeyMap::CONTEXT)}, 
    SKV {"RIGHT_CTRL", toByte(KeyMap::RIGHT_CTRL)}, SKV {"RIGHT_SHIFT", toByte(KeyMap::RIGHT_SHIFT)}, 
    SKV {"ENTER", toByte(KeyMap::ENTER)}, SKV {"BACKSPACE", toByte(KeyMap::BACKSPACE)}
  });

  // F-Keys
  this->defaultMapping()->registerMappings({
    SKV {"F1", toByte(KeyMap::F1)}, SKV {"F2", toByte(KeyMap::F2)},
    SKV {"F3", toByte(KeyMap::F3)}, SKV {"F4", toByte(KeyMap::F4)},
    SKV {"F5", toByte(KeyMap::F5)}, SKV {"F6", toByte(KeyMap::F6)},
    SKV {"F7", toByte(KeyMap::F7)}, SKV {"F8", toByte(KeyMap::F8)},
    SKV {"F9", toByte(KeyMap::F9)}, SKV {"F10", toByte(KeyMap::F10)},
    SKV {"F11", toByte(KeyMap::F11)}, SKV {"F12", toByte(KeyMap::F12)},
  });

  // Peripheral keys
  this->defaultMapping()->registerMappings({
    SKV {"PRINT", toByte(KeyMap::PRINT_SCREEN)},
    SKV {"SCROLL_LOCK", toByte(KeyMap::SCROLL_LOCK)},
    SKV {"PAUSE", toByte(KeyMap::PAUSE)},
  });

  // Position keys
  this->defaultMapping()->registerMappings({
    SKV {"INSERT", toByte(KeyMap::INSERT)}, SKV {"HOME", toByte(KeyMap::HOME)},
    SKV {"PAGE_UP", toByte(KeyMap::PAGE_UP)}, SKV {"DELETE", toByte(KeyMap::DELETE)},
    SKV {"END", toByte(KeyMap::END)}, SKV {"PAGE_DOWN", toByte(KeyMap::PAGE_DOWN)},

    SKV {"LEFT", toByte(KeyMap::LEFT)}, SKV {"RIGHT", toByte(KeyMap::RIGHT)},
    SKV {"UP", toByte(KeyMap::UP)}, SKV {"DOWN", toByte(KeyMap::DOWN)},
  });

  /* LOGITECH STUFF */

  // G-Keys
  this->defaultMapping()->registerMappings({
    SKV {"G1", toByte(KeyMap::G1)},
    SKV {"G2", toByte(KeyMap::G2)},
    SKV {"G3", toByte(KeyMap::G3)},
    SKV {"G4", toByte(KeyMap::G4)},
    SKV {"G5", toByte(KeyMap::G5)},
  });

  // Multimedia
  this->defaultMapping()->registerMappings({
    SKV {"PREV", toByte(KeyMap::MEDIA_PREV)},
    SKV {"PLAY", toByte(KeyMap::MEDIA_PLAY)},
    SKV {"NEXT", toByte(KeyMap::MEDIA_NEXT)},
    SKV {"MUTE", toByte(KeyMap::MEDIA_MUTE)},
  });

  this->defaultMapping()->registerMapping("DIMMER", toByte(KeyMap::DIM_BUTTON));
  this->defaultMapping()->registerMapping("LOGO", toByte(KeyMap::LOGO));
}

void G815::registerQUERTZ105Mapping() {
  // Special chars
  this->newMapping(KeyboardType::QUERTZ_105)->registerMappings({
    SKV {"^", toByte(KeyMap::BACK_TICK)},
    //SKV {"<", toInt(KeyMap::BACKSLASH)},

    SKV {"-", toByte(KeyMap::FORWARDSLASH)},

    SKV {"ö", toByte(KeyMap::SEMICOLON)},

    SKV {"ä", toByte(KeyMap::SINGLE_QUOTE)},
    
    SKV {"#", toByte(KeyMap::BACKSLASH)},
    
    SKV {"ü", toByte(KeyMap::OPEN_BRACKET)},

    SKV {"+", toByte(KeyMap::CLOSE_BRACKET)},
    
    SKV {"ß", toByte(KeyMap::DASH)},

    SKV {"´", toByte(KeyMap::EQUAL)},
    /* 
     * We need to override this as well since linux converts »´«
     * into »'« when no letter is assigned to it.
     * Thank You for your attention :)
     */
    SKV {"'", toByte(KeyMap::EQUAL)},
  });
}