#ifndef __G815_H__
#define __G815_H__

#include "utils.hpp"
#include "Keyboard.hpp"
#include "KeyboardInfo.hpp"

class G815 : public Keyboard {

  public:
    G815(const KeyboardInfo &kbdInfo);
    ~G815() {
      delete protocol;
    }
    
    bool setAllKeysColor(const int color);
    
    enum class KeyMap : uint8_t {
      A = 0x01, 
      B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U,
      V, W, X, Y, Z, N1, N2, N3, N4, N5, N6, N7, N8, N9, N0, ENTER,
      ESC, BACKSPACE, TAB, SPACE, DASH, EQUAL, OPEN_BRACKET, CLOSE_BRACKET,
      NUMBER_SIGN, BACKSLASH, SEMICOLON, SINGLE_QUOTE, BACK_TICK, COMMA, DOT, 
      FORWARDSLASH, CAPS, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, 
      PRINT_SCREEN, SCROLL_LOCK, PAUSE, INSERT, HOME, PAGE_UP, DELETE, END, 
      PAGE_DOWN, RIGHT, LEFT, DOWN, UP, NUM_LOCK, NUM_FORWARDSLASH, NUM_ASTERISK,
      NUM_MINUS, NUM_PLUS, NUM_ENTER, NUM1, NUM2, NUM3, NUM4, NUM5, NUM6, NUM7,
      NUM8, NUM9, NUM0, NUM_DOT, 
      CONTEXT = 0x62,
      LEFT_CTRL = 0x68, 
      LEFT_SHIFT, LEFT_ALT, 
      LEFT_WIN, RIGHT_CTRL, RIGHT_SHIFT, RIGHT_ALT, RIGHT_WIN, 
      
      DIM_BUTTON = 0x99,
      MEDIA_PLAY = 0x9b, 
      MEDIA_MUTE, MEDIA_NEXT, MEDIA_PREV, 
      
      G1 = 0xb4,
      G2, G3, G4, G5, 
      
      LOGO = 0xd2
    };

  protected:
    Protocol *getProtocol();
    std::vector<uint8_t> getAllKeys();

  private:
    Protocol * protocol;

    void registerMappings();
    void registerDefaultMapping();
    void registerQUERTZ105Mapping();

    uint8_t toByte(KeyMap key) {
      return static_cast<uint8_t>(key);
    }

};

#endif // __G815_H__