#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include <vector>
#include <cstdint>
#include <string>
#include <mutex>

#include "utils.hpp"
#include "KeyboardMapping.hpp"
#include "KeyboardInfo.hpp"
#include "HIDHandle.hpp"
#include "HIDConnector.hpp"
#include "Protocol.hpp"
#include "Animation.hpp"
#include "Tickable.hpp"

class Keyboard : public Tickable {

  public:
    Keyboard(
      const KeyboardMapping &def_map,
      const KeyboardInfo &kbdInfo
    ) : default_mapping(def_map), kbd_info(kbdInfo) {
      handle = HIDConnector::getHandle(&kbd_info);
    };

    ~Keyboard() {
      for (auto &mapping : mappings) {
        delete mapping.second;
      }
      
      mappings.clear();
      delete handle;
    }

    virtual bool close();
    virtual void tick(const uint64_t time, const uint64_t tickCount) override;
    virtual bool setKeyColor(uint8_t key, int color);
    virtual bool setRangeColor(uint8_t rangeStart, uint8_t rangeEnd, int color);
    virtual bool setVectorColor(const std::vector<uint8_t> &keys, int color);
    virtual bool setAllKeysColor(const int color) = 0;
    virtual bool blackout();
    virtual bool commit();
    void addAnimation(Animation *anim);
    virtual std::vector<uint8_t> getAllKeys() = 0;
    virtual uint8_t mapKey(const KeyboardType &kbdType, const std::string &key);
    virtual KeyboardMapping * defaultMapping();
    virtual KeyboardMapping * newMapping(const KeyboardType &kbType);
    static Keyboard * instantiate(const KeyboardInfo &keyboard);
    
  protected:
    virtual Protocol *getProtocol() = 0;

  private:
    const KeyboardInfo kbd_info;
    KeyboardMapping default_mapping;
    std::mutex animLock;
    std::vector<Animation*> animations = {};
    std::map<KeyboardType, KeyboardMapping*> mappings = {};
    HIDHandle *handle;

};

#endif // __KEYBOARD_H__