#ifndef __BUFFERBUILDER_H__
#define __BUFFERBUILDER_H__

#include "utils.hpp"

class BufferBuilder {
  public:
    BufferBuilder(const uint8_t terminationByte) : termination(terminationByte) {};
    virtual BufferBuilder *action(const uint8_t action);
    virtual BufferBuilder *push_back(const uint8_t byte);
    virtual BufferBuilder *color(const int color);
    byte_buffer_t finish(const bool terminate = false);

    static BufferBuilder *create(const int address, const uint8_t terminationByte) {
      BufferBuilder *bb = new BufferBuilder(terminationByte);

      bb->push_back(static_cast<uint8_t>((address >> 16) & 0xFF));
      bb->push_back(static_cast<uint8_t>((address >> 8) & 0xFF));
      bb->push_back(static_cast<uint8_t>(address & 0xFF));

      return bb;
    }
  protected:
    const uint8_t termination;
    byte_buffer_t current;

};

#endif // __BUFFERBUILDER_H__