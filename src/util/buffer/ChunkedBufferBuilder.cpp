#include "ChunkedBufferBuilder.hpp"


ChunkedBufferBuilder* ChunkedBufferBuilder::push_back(const uint8_t byte) {
  checkNewBuffer();

  super::push_back(byte);
  return this;
}

ChunkedBufferBuilder* ChunkedBufferBuilder::push_back(const std::vector<uint8_t> &vec) {
  auto pos = 0;
  const auto size = vec.size();

  while (pos < size) {
    const int space_left = checkNewBuffer();
    const int to = (space_left > size - pos) ? size : pos + space_left;
    int size = super::current.size();

    super::current.insert(super::current.end(), vec.begin() + pos, vec.begin() + to);

    size = super::current.size();

    pos = to;
  }

  return this;
}

ChunkedBufferBuilder* ChunkedBufferBuilder::lockTemplate() {
  tpl = super::current;

  return this;
}


std::vector<byte_buffer_t> ChunkedBufferBuilder::finishChunked(const bool terminate) {
  if (terminate && super::current.size() < 20) {
    super::action(super::termination);
  }

  super::current.resize(chunk_size, 0x00);
  bufVec.push_back(super::current);
  
  return bufVec;
}

int ChunkedBufferBuilder::checkNewBuffer() {
  int space_left = chunk_size - super::current.size();
  if (space_left == 0) {
    bufVec.push_back(super::current);
    super::current = tpl;
    space_left = chunk_size - super::current.size() ;
  }

  return space_left;
}