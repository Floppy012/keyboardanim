#include "BufferBuilder.hpp"

BufferBuilder* BufferBuilder::action(const uint8_t action) {
  push_back(action);

  return this;
}

BufferBuilder* BufferBuilder::push_back(const uint8_t byte) {
  current.push_back(byte);

  return this;
}

BufferBuilder* BufferBuilder::color(const int color) {
  push_back(static_cast<uint8_t>((color >> 16) & 0xFF));
  push_back(static_cast<uint8_t>((color >> 8) & 0xFF));
  push_back(static_cast<uint8_t>(color & 0xFF));

  return this;
}

byte_buffer_t BufferBuilder::finish(const bool terminate) {
  if (terminate && current.size() < 20) {
    action(termination);
  }
  
  current.resize(20, 0x00);
  return current;
}