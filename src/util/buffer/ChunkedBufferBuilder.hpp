#ifndef __CHUNKEDBUFFERBUILDER_H__
#define __CHUNKEDBUFFERBUILDER_H__

#include "BufferBuilder.hpp"

class ChunkedBufferBuilder : BufferBuilder {

  public:
    ChunkedBufferBuilder(const int chunkSize, const uint8_t terminationByte) : BufferBuilder(terminationByte), chunk_size(chunkSize) {};
    ChunkedBufferBuilder *push_back(const uint8_t byte) override;
    ChunkedBufferBuilder *push_back(const std::vector<uint8_t> &vec);
    ChunkedBufferBuilder *lockTemplate();
    std::vector<byte_buffer_t> finishChunked(const bool terminate = false);

    static ChunkedBufferBuilder *create(const int chunkSize, const int address, const uint8_t terminationByte) {
      ChunkedBufferBuilder *bb = new ChunkedBufferBuilder(chunkSize, terminationByte);

      bb->push_back(static_cast<uint8_t>((address >> 16) & 0xFF));
      bb->push_back(static_cast<uint8_t>((address >> 8) & 0xFF));
      bb->push_back(static_cast<uint8_t>(address & 0xFF));

      return bb;
    }

    /*
     * Override methods directly that have no different code
     * but whose return value needs to be casted to ChunkedBufferBuilder
     */

    ChunkedBufferBuilder *action(const uint8_t action) override {
      return (ChunkedBufferBuilder*) super::action(action);
    }

    ChunkedBufferBuilder *color(const int color) override {
      return (ChunkedBufferBuilder*) super::color(color);
    }

  private:
    typedef BufferBuilder super;
    const int chunk_size;
    std::vector<byte_buffer_t> bufVec;
    byte_buffer_t tpl;

    int checkNewBuffer();


};

#endif // __CHUNKEDBUFFERBUILDER_H__