#ifndef __UTILS_H__
#define __UTILS_H__

#include <iostream>
#include <cstdint>
#include <vector>
#include <string>
#include <chrono>
#include <thread>

typedef std::vector<unsigned char> byte_buffer_t;
typedef std::string string;

template <typename K, typename V>
struct KeyValue {
  K key;
  V value;
};

template <typename K, typename V>
using KeyValueArray = std::vector<KeyValue<K, V>>;

struct Utils {
  static std::string fromWchar(const wchar_t *wchar) {
    char buffer[256];
    wcstombs(buffer, wchar, 256);
    return string(buffer);
  }

  static void addRange(const uint8_t from, const uint8_t to, std::vector<uint8_t> &out) {
    for (uint8_t n = from; n <= to; n++) {
      out.push_back(n);
    }
  }

  template<typename T>
  static void coutVector(std::vector<T> vec, bool hex = false) {
    std::cout << "std::vector {";
    int n = 0;
    for (const T entry : vec) {
      if (hex) {
        std::cout << "0x" << std::hex << static_cast<int>(entry);
      } else {
        std::cout << entry;
      }

      if (hex) {
        std::cout << std::dec;
      }

      if (++n < vec.size()) {
        std::cout << ", ";
      }
    }

    std::cout << "}";
  }

  static int64_t currentMS() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::system_clock::now().time_since_epoch()
    ).count();
  }
};
#endif // __UTILS_H__