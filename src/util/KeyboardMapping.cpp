#include "KeyboardMapping.hpp"

std::map<std::string, uint8_t> * KeyboardMapping::getKeyMappings() {
  return &key_mappings;
}

void KeyboardMapping::registerMapping(const std::string &key, const uint8_t mapping) {
  key_mappings[key] = mapping;
}

void KeyboardMapping::registerMappings(const std::vector<MappingEntry> &mappings) {
  for (auto &mapping : mappings) {
    registerMapping(mapping.key, mapping.value);
  }
}

bool KeyboardMapping::containsMapping(const std::string &key) {
  return key_mappings.find(key) != key_mappings.end();
}

uint8_t KeyboardMapping::map(const std::string &key) {
  auto it = key_mappings.find(key);
  if (it == key_mappings.end()) {
    if (!parent) {
      throw std::range_error("No mapping found for key: " + key);
    }
    
    return parent->map(key);
  }

  return it->second;
}