#ifndef __KEYBOARDMAPPING_H__
#define __KEYBOARDMAPPING_H__

#include <map>

#include "utils.hpp"
#include "KeyboardType.hpp"

typedef KeyValue<std::string, uint8_t> MappingEntry;
typedef MappingEntry SKV;

class KeyboardMapping {
  
  public:
    KeyboardMapping(const KeyboardType kbType, KeyboardMapping *parent) : type(kbType), parent(parent) {};

    virtual std::map<std::string, uint8_t> * getKeyMappings();
    virtual void registerMapping(const std::string &key, uint8_t mapping);
    virtual void registerMappings(const std::vector<MappingEntry> &mappings);
    virtual bool containsMapping(const std::string &key);
    virtual uint8_t map(const std::string &key);
  
  private:
    KeyboardMapping *parent;
    KeyboardType type;
    std::map<std::string, uint8_t> key_mappings = {};

};


#endif // __KEYBOARDMAPPING_H__