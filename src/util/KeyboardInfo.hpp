#ifndef __KEYBOARDINFO_H__
#define __KEYBOARDINFO_H__

#include "utils.hpp"

enum GKeyboards : uint16_t {
  UNKNOWN,
  G815_KBD = 0xc33f,
};

struct KeyboardInfo {
  uint16_t vendorID = 0x0;
  uint16_t productID = 0x0;
  string manufacturer = "";
  string product = "";
  string serialNumber = "";
  string path = "";

  friend std::ostream &operator<<(std::ostream &os, const KeyboardInfo &info) {
    os << "Device: " << info.product << std::endl; 
    os << "  VendorID: " << std::hex << info.vendorID << std::endl;
    os << "  ProductID: " << std::hex << info.productID << std::endl;
    os << "  Manufacturer: " << info.manufacturer << std::endl;
    os << "  Serial Number: " << info.serialNumber << std::endl;
    os << "  Path: " << info.path << std::endl;
    return os;
  }
};


#endif // __KEYBOARDINFO_H__