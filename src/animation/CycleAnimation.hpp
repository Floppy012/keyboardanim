#ifndef __CYCLEANIMATION_H__
#define __CYCLEANIMATION_H__

#include "Animation.hpp"
#include "Keyboard.hpp"

class CycleAnimation : public Animation {

  public:
    CycleAnimation(std::vector<uint8_t> animKeys) : Animation(animKeys) {};
    void tick(const uint64_t time, const uint64_t tickCount, Keyboard *kbd) override;

  private:
    void nextColor();

  private:
    int current_color = 0xFF0000;
    uint8_t mode;
};

#endif // __CYCLEANIMATION_H__