#include "CycleAnimation.hpp"
#include "AnimationThread.hpp"


void CycleAnimation::tick(const uint64_t time, const uint64_t tickCount, Keyboard *kbd) {
  kbd->setVectorColor(Animation::keys, current_color);
  kbd->commit();
  nextColor();
}


void CycleAnimation::nextColor() {

  switch (mode) {
    case 0: // R -> Increase G
      current_color += 0x00500;
      if (current_color == 0xFFFF00) mode++;
      break;
    case 1: // RG -> Decrease R
      current_color -= 0x050000;
      if (current_color == 0x00FF00) mode++;
      break;
    case 2: // G -> Increase B
      current_color += 0x5;
      if (current_color == 0x00FFFF) mode++;
      break;
    case 3: // GB -> Decrease G
      current_color -= 0x000500;
      if (current_color == 0x0000FF) mode++;
      break;
    case 4: // B -> Increase R
      current_color += 0x050000;
      if (current_color == 0xFF00FF) mode++;
      break;
    case 5: // BR -> Decrease B
      current_color -= 0x5;
      if (current_color == 0xFF0000) mode = 0;
      break;
  }
  
}