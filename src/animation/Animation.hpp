#ifndef __ANIMATION_H__
#define __ANIMATION_H__

#include "utils.hpp"

class Keyboard;

class Animation {

  public:
    Animation(std::vector<uint8_t> keys) : keys(keys) {};
    virtual void tick(const uint64_t time, const uint64_t tickCount, Keyboard *kbd) = 0;

  protected:
    std::vector<uint8_t> keys;

};

#endif // __ANIMATION_H__