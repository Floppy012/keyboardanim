#include "BlinkAnimation.hpp"
#include "Keyboard.hpp"


void BlinkAnimation::tick(const uint64_t time, const uint64_t tickCount, Keyboard *kbd) {

  kbd->setAllKeysColor(currentOn ? 0 : color);
  kbd->commit();

  currentOn = !currentOn;

}
