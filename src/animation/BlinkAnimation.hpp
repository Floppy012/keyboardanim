#ifndef __BLINKANIMATION_H__
#define __BLINKANIMATION_H__

#include "Animation.hpp"

class BlinkAnimation : public Animation {

  public:
    BlinkAnimation(std::vector<uint8_t> animKeys, const int color) : Animation(animKeys), color(color) {};
    void tick(const uint64_t time, const uint64_t tickCount, Keyboard *kbd) override;

  private:
    const int color;
    bool currentOn;

};


#endif // __BLINKANIMATION_H__