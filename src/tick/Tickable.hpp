#ifndef __TICKABLE_H__
#define __TICKABLE_H__

#include "utils.hpp"

class Tickable {

  public:
    virtual void tick(const uint64_t time, const uint64_t tickCount) = 0;

};

#endif // __TICKABLE_H__