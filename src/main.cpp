#include <iostream>
#include <string>

#include "HIDConnector.hpp"
#include "Keyboard.hpp"
#include "CycleAnimation.hpp"
#include "BlinkAnimation.hpp"
#include "AnimationThread.hpp"

int main(int argc, char *argv[]) {
  try {
    HIDConnector::initHID();

    std::vector<KeyboardInfo> kbds = HIDConnector::getAvailableKeyboards();
    KeyboardInfo info = kbds[1];

    AnimationThread *animThread = AnimationThread::getInstance();
    std::thread *thread = animThread->start();

    Keyboard *kbd = Keyboard::instantiate(info);
    animThread->addTickable(kbd);

    Animation *anim = new BlinkAnimation(kbd->getAllKeys(), 0xFFFFFF);
    kbd->addAnimation(anim);

    thread->join();
    std::cout << "Hello world!" << std::endl;
  } catch (...) {
    HIDConnector::exitHID();
  }
  
}