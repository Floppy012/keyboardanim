#ifndef __ANIMATIONTHREAD_H__
#define __ANIMATIONTHREAD_H__

#include <mutex>

#include "Thread.hpp"
#include "Tickable.hpp"

class AnimationThread : public Thread {

  public:
    void addTickable(Tickable *tickable);
    void removeTickable(Tickable *tickable);

    void onStart() override {};
    void onTick(const unsigned long currentTime, const unsigned long tickCount) override;
    void onStop() override {};


    static AnimationThread *getInstance();

  private:
    AnimationThread() : Thread("AnimationThread", 60, QUEUE_BEFORE_TICK, 16) {};
    std::mutex tickableMutex;
    std::vector<Tickable*> tickables;

};

#endif // __ANIMATIONTHREAD_H__