#include "AnimationThread.hpp"

void AnimationThread::onTick(const unsigned long currentTime, const unsigned long tickCount) {
  tickableMutex.lock();
  for (Tickable * tickable : tickables) {
    tickable->tick(currentTime, tickCount);
  }
  tickableMutex.unlock();
}

void AnimationThread::addTickable(Tickable *tickable) {
  if (std::find(tickables.begin(), tickables.end(), tickable) != tickables.end()) return;
  tickableMutex.lock();
  tickables.push_back(tickable);
  tickableMutex.unlock();
}


void AnimationThread::removeTickable(Tickable *tickable) {
  tickableMutex.lock();
  tickables.erase(std::remove(tickables.begin(), tickables.end(), tickable), tickables.end());
  tickableMutex.unlock();
}


AnimationThread *AnimationThread::getInstance() {
  static AnimationThread *thread = new AnimationThread();
  return thread;
}